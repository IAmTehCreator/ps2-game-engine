#ifndef __GAME_HEADER__
#define __GAME_HEADER__

#include "engine/PS2Game.h"

class Game : public PS2Game {
protected:
	void onTick ();
};

#endif
