#include "VUMicroprogram.h"
#include "memory/DMAChain.h"
#include "memory/VIFStaticDMA.h"
#include "memory/VIFDynamicDMA.h"
#include "PS2Manager.h"

VUMicroprogram::VUMicroprogram (uint64 *startPointer, uint64 *endPointer, int offset) {
	VIFStaticDMA staticPacket PS2.getVU1().getStaticChain();

	this->uploadPointer = staticPacket.getPointer();
	this->size = endPointer - startPointer;
	this->offset = offset;

	staticPacket.startMicrocodeUpload();
	for ( int i = 0; i < this->size; i++ ) {
		staticPacket.addMicrocode( this->startPointer[i] );
	}
	staticPacket.endMicrocodeUpload();

	staticPacket.DMAReturn();
}

VUMicroprogram::~VUMicroprogram () {}

void VUMicroprogram::upload () {
	VIFDynamicDMA dynamicPacket PS2.getVU1().getDynamicChain();

	dynamicPacket.add32(FLUSHE);
	dynamicPacket.DMACall( this->uploadPointer );
}

int VUMicroprogram::getLengthDW () {
	return this->offset;
}
