#ifndef __SPS2_WRAPPER_HEADER__
#define __SPS2_WRAPPER_HEADER__

#define IMAGE_RGBA	(0)
#define IMAGE_RGB	(1)

#define VU0_DRIVER "/dev/ps2vpu0"
#define VU1_DRIVER "/dev/ps2vpu1"

#define SPS2 SPS2Wrapper::getSingleton()

#include <sps2lib.h>
#include <sps2util.h>
#include <sps2tags.h>
#include "Singleton.h"
#include "memory/MemoryPage.h"

class SPS2Wrapper : public Singleton<SPS2Wrapper> {
public:
	SPS2Wrapper ();
	~SPS2Wrapper ();

	void initialise (const int iAllocPages);
	void initClearScreen (int red, int green, int blue);

	void beginScene ();
	void endScene ();

	void allocate (MemoryPage &memory, int quadWords);
	void flushCache ();

	int getScreenWidth ();
	int getScreenHeight ();

	void screenshot (const int type = 1);
private:
	int m_iSPS2Desc;

	// The memory that is allocated by Initialise( )
	sps2Memory_t *cachedMemory, *uncachedMemory;

	// Keep an array with the memory remaining in each page
	int *freePageMemory;
	// How many pages worth of memory did we allocate?
	int pageCount;

	int screenClearPacket;

	// Screen Shot variables
	int screenshotNumber;

	// Screen shot functions
	// Check if a file exists - used to get a free filename for the screen shot
	int fileExists(const char * const FileName);
	// Download part of the frame buffer to main memory
	void downloadVram(MemoryPage &dmaChainMemory, MemoryPage &screenMemory, int addr, int bufw, int x, int y, int w, int h);
};

#endif
