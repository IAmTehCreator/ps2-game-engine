#ifndef __PS2_MATHS_HEADER__
#define __PS2_MATHS_HEADER__

#define Maths PS2Maths::getSingleton()

#include "Singleton.h"

#define TWOPI 6.283185307179586476925286766559f
#define PI 3.1415926535897932384626433832795f
#define PIHALF 1.5707963267948966192313216916398f
#define PIDIV4 0.78539816339744830961566084581988f

class PS2Maths : public Singleton<PS2Maths> {
public:
	float abs (const float x);
	float sqrt (const float x);
	float max (const float a, const float b);
	float min (const float a, const float b);
	float mod (const float a, const float b);
	float aSin (float x);
	float aCos (float x);
	float cos (float v);
	float sin (float v);
	float degToRad (float Deg);
};

#endif
