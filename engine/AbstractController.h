#ifndef __ABSTRACT_CONTROLLER_HEADER__
#define __ABSTRACT_CONTROLLER_HEADER__

#include "devices/ControllerDevice.h"

class AbstractController {
public:
	AbstractController (ControllerDevice *device);
	~AbstractController ();

	void tick ();
protected:
	virtual void onTick ();

	virtual void onUpPressed ();
	virtual void onRightPressed ();
	virtual void onDownPressed ();
	virtual void onLeftPressed ();

	virtual void onSelectPressed ();
	virtual void onStartPressed ();

	virtual void onTrianglePressed ();
	virtual void onCirclePressed ();
	virtual void onCrossPressed ();
	virtual void onSquarePressed ();

	virtual void onR1Pressed ();
	virtual void onR2Pressed ();
	virtual void onR3Pressed ();

	virtual void onL1Pressed ();
	virtual void onL2Pressed ();
	virtual void onL3Pressed ();

	// TODO: Analogue sticks
private:
	ControllerDevice *device;
};

#endif
