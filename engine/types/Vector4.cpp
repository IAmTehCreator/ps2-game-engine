#include "Vector4.h"

Vector4::Vector4 () {}
Vector4::Vector4 (const float x, const float y, const float z, const float w) : x(x),y(y),z(z),w(w) {}
Vector4::Vector4 (const Vector4 &rhs) : x(rhs.x),y(rhs.y),z(rhs.z),w(rhs.w) {}

Vector4::~Vector4 () {}

bool Vector4::operator == (const Vector4 &rhs) const {
	return ((x == rhs.x) && (y == rhs.y) && (z == rhs.z) && (w == rhs.w));
}

Vector4 & Vector4::operator += (const Vector4 &rhs) {
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;
	w = 1.0f;

	return *this;
}

Vector4 & Vector4::operator -= (const Vector4 &rhs) {
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;
	w = 1.0f;

	return *this;
}

Vector4 & Vector4::operator *= (const float s) {
	x *= s;
	y *= s;
	z *= s;
	w  = 1.0f;

	return *this;
}

Vector4 & Vector4::operator /= (const float s) {
	x /= s;
	y /= s;
	z /= s;
	w = 1.0f;;

	return *this;
}

float Vector4::dot4 (const Vector4 &rhs) const {
	return (x * rhs.x + y * rhs.y + z * rhs.z + w * rhs.w);
}

float Vector4::dot3 (const Vector4 &rhs) const {
	return (x * rhs.x + y * rhs.y + z * rhs.z);
}

Vector4 Vector4::cross (const Vector4 &rhs) const {
	return Vector4(y * rhs.z - z * rhs.y, z * rhs.x - x * rhs.z, x * rhs.y - y * rhs.x, 1.0f);
}

float Vector4::length () const {
	return Sqrt(x * x + y * y + z * z);
}

float Vector4::lengthSqr() const {
	return (x * x + y * y + z * z);
}

Vector4 Vector4::normalise () {
	return (*this / this->length());
}

void Vector4::normaliseSelf () {
	*this /= this->length();
}

void Vector4::dumpVector4 (char *s) {
	if (s != NULL) printf("\n%f %f %f %f %s\n\n", x, y, z, w, s);
	else printf("\n%f %f %f %f\n\n", x, y, z, w);
}
