#ifndef __VECTOR4_CHAIN__
#define __VECTOR4_CHAIN__

#include <stdio.h>
#include "../PS2Maths.h"

class Vector4 {
public:
	Vector4 ();
	Vector4 (const float x, const float y, const float z, const float w);
	Vector4 (const Vector4 &rhs);

	~Vector4 ();

	// Properties
	float x;
	float y;
	float z;
	float w;

	// Operator overloads
	bool operator == (const Vector4 & rhs) const;
	Vector4 & operator += (const Vector4 &rhs);
	Vector4 & operator -= (const Vector4 &rhs);
	Vector4 & operator *= (const float s);
	Vector4 & operator /= (const float s);

	// Special Arithmetic
	float dot3 (const Vector4 &rhs) const;
	float dot4 (const Vector4 &rhs) const;
	Vector4 cross (const Vector4 & rhs) const;

	float length () const;
	float lengthSqr () const;
	Vector4 normalise ();
	void normaliseSelf ();
	void dumpVector4 (char *s);
};

inline Vector4 operator + (const Vector4 &v1, const Vector4 &v2) {
	return Vector4(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, 1.0f);
}

inline Vector4 operator - (const Vector4 &v1, const Vector4 &v2) {
	return Vector4(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, 1.0f);
}

inline Vector4 operator - (const Vector4 &v1) {
	return Vector4(-v1.x, -v1.y, -v1.z, 1.0f);
}

inline Vector4 operator * (const Vector4 &v, const float &s) {
	return Vector4(v.x * s, v.y * s, v.z * s, 1.0f);
}

inline Vector4 operator * (const float & s, const Vector4 &v) {
	return Vector4(v.x * s, v.y * s, v.z * s, 1.0f);
}

inline Vector4 operator / (const Vector4 &v, const float & s) {
	return Vector4(v.x / s, v.y / s, v.z / s, 1.0f);
}

inline Vector4 operator * (const Vector4 &v1, const Vector4 &v2) {
	return Vector4(v1.y * v2.z - v1.z * v2.y,
				   v1.z * v2.x - v1.x * v2.z,
				   v1.x * v2.y - v1.y * v2.x,
				   1.0f);
}

inline Vector4 CrossProduct (const Vector4 &v1, const Vector4 &v2) {
	return Vector4(v1.y * v2.z - v1.z * v2.y,
				   v1.z * v2.x - v1.x * v2.z,
				   v1.x * v2.y - v1.y * v2.x,
				   1.0f);
}

inline float DotProduct4 (const Vector4 &v1, const Vector4 &v2) {
	return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z + v1.w * v2.w);
}

inline float DotProduct3 (const Vector4 &v1, const Vector4 &v2) {
	return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
}

inline Vector4 Normalise (const Vector4 &v) {
	return v / v.length();
}

#endif
