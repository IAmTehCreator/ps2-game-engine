#include "SPS2Wrapper.h"

SPS2Wrapper::SPS2Wrapper () {
	printf("Writing cache to HDD\n");
	system("sync");

	// The array to keep track of how much memory is allocated in each page
	freePageMemory = 0;
	screenClearPacket = 0;
}

SPS2Wrapper::~SPS2Wrapper ()
{
	sps2UScreenShutdown();
	sps2Release(m_iSPS2Desc);

	// Free up all of the memory that was allocated in Initialise
	if (freePageMemory) {
		delete [] freePageMemory;
		freePageMemory = 0;
	}
}

void SPS2Wrapper::initialise (const int iAllocPages)
{
	// Keep track of how many 4K pages will be allocated
	this->pageCount = iAllocPages;

	// Initialise SPS2
	this->m_iSPS2Desc = sps2Init();

	if (m_iSPS2Desc < 0) {
		fprintf(stderr, "Failed to initialise SPS2 library. Please check that the module is loaded.\n");
		exit(-1);
	}

	// Open the vector units
	int iVPU0; iVPU0 = open(VU0_DRIVER, O_RDWR);
	int iVPU1; iVPU1 = open(VU1_DRIVER, O_RDWR);

	// Allocate the memory that we will be using, and get the cached and cached pointers.
	this->cachedMemory = sps2Allocate(iAllocPages * 4096, SPS2_MAP_BLOCK_4K | SPS2_MAP_CACHED, m_iSPS2Desc);
	this->uncachedMemory = sps2Remap(this->cachedMemory, SPS2_MAP_UNCACHED, m_iSPS2Desc);

	// Set up an array to keep track of how much memory is free in each page
	this->freePageMemory = new int[this->pageCount];

	for(int iPage = 0; iPage < this->pageCount; iPage++) {
		this->freePageMemory[iPage] = 256;	// 4K == 256 Quad words
	}

	// Set up the screen
	sps2UScreenInit(0);

	// Enable EI DI instructions (this is needed for the screen shot code)
	_sps2SetEIDIEnabled(1, m_iSPS2Desc);
}

void SPS2Wrapper::allocate (MemoryPage &memory, int quadWords) {
	for(int iPage = 0; iPage < this->pageCount; iPage++) {
		// Find the first page with enough free memory in it.
		if (this->freePageMemory[iPage] >= quadWords) {
			// Get the first free quad word in the page.
			int FirstQW = 256 - this->freePageMemory[iPage];
			this->freePageMemory[iPage] -= quadWords;

			// Get the pointers.
			void *pCached = &((char *)cachedMemory->pvStart)[iPage * 4096 + (FirstQW * 16)];
			void *pUncached = &((char *)uncachedMemory->pvStart)[iPage * 4096 + (FirstQW * 16)];

			// Set the CDMAMem's member variables
			memory.set(quadWords, pCached, pUncached, sps2GetPhysicalAddress(pCached, cachedMemory));
			return;
		}
	}

	assert(!"Not enough free memory!");
}

void SPS2Wrapper::initScreenClear (int red, int green, int blue) {
	VIFStaticDMA staticChain = PS2.getVU1().getStaticChain();

	int x0 = (2048 - (sps2UScreenGetWidth() >> 1)) << 4;
	int y0 = (2048 - (sps2UScreenGetHeight() >> 1)) << 4;
	int y1 = (2048 + (sps2UScreenGetHeight() >> 1)) << 4;

	// Get the address of the screen clear packet so we can CALL it from the dynamic packet.
	screenClearPacket = staticChain.getPointer();

	// Start the VIF direct mode.
	staticChain.startDirect();

	staticChain.add128(GS_GIFTAG_BATCH(4 + (20 * 2), 1, 0, 0, GIF_FLG_PACKED, GS_BATCH_1(GIF_REG_A_D)));

	staticChain.add64(TEST_SET(0, 0, 0, 0, 0, 0, 1, 1));
	staticChain.add64(TEST_1);

	staticChain.add64(PRIM_SET(0x6, 0, 0, 0, 0, 0, 0, 0, 0));
	staticChain.add64(PRIM);

	staticChain.add64(RGBAQ_SET(red, green, blue, 0x80, 0x3f800000));
	staticChain.add64(RGBAQ);

	for (int i = 0; i < 20; i++) {
		staticChain.add64(XYZ2_SET(x0, y0, 0));
		staticChain.add64(XYZ2);

		staticChain.add64(XYZ2_SET(x0 + (32 << 4), y1, 0));
		staticChain.add64(XYZ2);

		x0 += (32 << 4);
	}

	staticChain.add64(TEST_SET(0, 0, 0, 0, 0, 0, 1, 3));
	staticChain.add64(TEST_1);

	staticChain.endDirect();
	staticChain.DMAReturn();
}

void SPS2Wrapper::beginScene () {
	PS2.getVU1().getDynamicChain().DMACall(screenClearPacket);
}

void SPS2Wrapper::flushCache () {
	sps2FlushCache(m_iSPS2Desc);
}

void SPS2Wrapper::endScene () {
	while(*EE_D1_CHCR & 256);
	sps2UScreenSwap();
}

// Flushes all data through SPS2 to make sure it is written to memory before
// it is fired off to the DMAC.
void SPS2Wrapper::flushCache () {
	sps2FlushCache(m_iSPS2Desc);
}
