#ifndef __VECTOR_UNIT_DEVICE_HEADER__
#define __VECTOR_UNIT_DEVICE_HEADER__

#include "../memory/VIFDynamicDMA.h"
#include "../memory/VIFStaticDMA.h"

class VectorUnitDevice {
public:
	VectorUnitDevice ();
	~VectorUnitDevice ();

	VIFDynamicDMA getDynamicChain ();
	VIFStaticDMA getStaticChain ();

private:
	VIFDynamicDMA dynamicDma;
	VIFStaticDMA staticDma;
};

#endif
