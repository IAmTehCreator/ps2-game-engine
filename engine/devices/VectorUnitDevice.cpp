#include "VectorUnitDevice.h"

VectorUnitDevice::VectorUnitDevice () {}
VectorUnitDevice::~VectorUnitDevice () {}

VIFDynamicDMA VectorUnitDevice::getDynamicChain () {
	return this->dynamicDma;
}

VIFStaticDMA VectorUnitDevice::getStaticChain () {
	return this->staticDma;
}
