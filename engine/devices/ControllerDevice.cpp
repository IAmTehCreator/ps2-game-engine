#include "ControllerDevice.h"

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <math.h>
#include <linux/ps2/dev.h>
#include <memory.h>

#define PACKED	__attribute__((packed))
#define DRIFT_TOLERANCE	30
typedef unsigned char u8;
typedef signed char s8;
typedef unsigned short u16;

struct DigitalData {
	u8 	Unknown				PACKED;
	u8 	Flags		: 4		PACKED;
	u8 	Type		: 4		PACKED;
	u16	Buttons				PACKED;
} __attribute__((aligned(2)));

struct DualShock2Data {
	u8 	Unknown			PACKED;
	u8 	Flags		: 4		PACKED;
	u8 	Type		: 4		PACKED;
	u16	Buttons				PACKED;
	u8	RsX					PACKED;
	u8	RsY					PACKED;
	u8	LsX					PACKED;
	u8	LsY					PACKED;
	u8	PressureRight		PACKED;
	u8	PressureLeft		PACKED;
	u8	PressureUp			PACKED;
	u8	PressureDown		PACKED;
	u8	PressureTriangle	PACKED;
	u8	PressureCircle		PACKED;
	u8	PressureCross		PACKED;
	u8	PressureSquare		PACKED;
	u8	PressureL1			PACKED;
	u8	PressureR1			PACKED;
	u8	PressureL2			PACKED;
	u8	PressureR2			PACKED;
} __attribute__((aligned(2)));

union ControllerData {
	u8 Buffer[PS2PAD_DATASIZE];
	struct DigitalData Digital;
	struct DualShock2Data DualShock2;
};

ControllerDevice::ControllerDevice () {}
ControllerDevice::~ControllerDevice () {}

int ControllerDevice::init ( int port, int flags ) {
	const char *devices[2] = { PS2_DEV_PAD0, PS2_DEV_PAD1 };
	int mode, lock;
	int status;

	this->port = port;
	this->flags = flags;

	this->fileHandle = open( devices[this->port], O_RDONLY );
	if ( fileHandle < 0 ) {
		log("Could not open handle to controller");
		return 0;
	}

	status = waitForStatusReady();
	if ( status != PS2PAD_STAT_READY ) {
		log("Not connected");
		closeHandle();
		return 1;
	}

	mode = (flags & PAD_INIT_ANALOGUE) ? 1 : 0;
	lock = (flags & PAD_INIT_LOCK) ? 1 : 0;
	if ( !setMode(mode, lock) ) {
		log("Could not set the controller mode");
		return closeHandle();
	}

	waitForStatusReady();
	if ( (flags & PAD_INIT_ANALOGUE) && (flags & PAD_INIT_PRESSURE) && isPressureSupported() ) {
		if ( !enablePressure() ) {
			log("Could not enable pressure sensing on a controller");
			setMode(0, 0);
			return closeHandle();
		}

		waitForStatusReady();
	}

	memset( &this->state, 0, sizeof(ControllerState) );
	this->state.InitFlags = flags;
	this->state.Actuator = isActuatorSupported();

	log("Connected");
	return 1;
}

void ControllerDevice::update () {
	union ControllerData data;
	int n;
	int changed, last;
	int status;

	if (this->fileHandle < 0) {
		return;
	}

	do {
		status = getStatus();
	} while (status == PS2PAD_STAT_BUSY);

	n = read( this->fileHandle, data.Buffer, sizeof(data) );
	if ( n <= 0 ) {
		return;
	}

	//update the buttons
	last = this->state.Buttons;

	//buttons are inverted!
	this->state.Buttons = ~data.Digital.Buttons;

	changed = last ^ ~data.Digital.Buttons;
	this->state.Pressed = this->state.Buttons & changed;
	this->state.Released = last & changed;

	if ( this->flags & PAD_INIT_ANALOGUE ) {
		this->state.Axes[PAD_AXIS_LX] = axis2float( data.DualShock2.LsX );
		this->state.Axes[PAD_AXIS_LY] = axis2float( data.DualShock2.LsY );
		this->state.Axes[PAD_AXIS_RX] = axis2float( data.DualShock2.RsX );
		this->state.Axes[PAD_AXIS_RY] = axis2float( data.DualShock2.RsY );

		if (this->flags & PAD_INIT_PRESSURE) {
			this->state.Pressures[PAD_PCROSS] = pressure2float( data.DualShock2.PressureCross );
			this->state.Pressures[PAD_PTRI] = pressure2float( data.DualShock2.PressureTriangle );
			this->state.Pressures[PAD_PCIRCLE] = pressure2float( data.DualShock2.PressureCircle );
			this->state.Pressures[PAD_PSQUARE] = pressure2float( data.DualShock2.PressureSquare );
			this->state.Pressures[PAD_PUP] = pressure2float( data.DualShock2.PressureUp );
			this->state.Pressures[PAD_PDOWN] = pressure2float( data.DualShock2.PressureDown );
			this->state.Pressures[PAD_PLEFT] = pressure2float( data.DualShock2.PressureLeft );
			this->state.Pressures[PAD_PRIGHT] = pressure2float( data.DualShock2.PressureRight );
			this->state.Pressures[PAD_PR1] = pressure2float( data.DualShock2.PressureR1 );
			this->state.Pressures[PAD_PR2] = pressure2float( data.DualShock2.PressureR2 );
			this->state.Pressures[PAD_PL1] = pressure2float( data.DualShock2.PressureL1 );
			this->state.Pressures[PAD_PL2] = pressure2float( data.DualShock2.PressureL2 );
		}
	}
}

/**
 * Returns the current state of the controler.
 * @return The state of the controller
 */
ControllerState ControllerDevice::getState () {
	return this->state;
}

/**
 * Returns 1 if the specified button has just been pressed.
 */
int ControllerDevice::isPressed ( int button ) {
	return this->state.Pressed & button;
}

/**
 * Returns 1 if the specified button is currently down
 */
int ControllerDevice::isDown ( int button ) {
	return this->state.Buttons & button;
}

/**
 * Returns 1 if the specified button has just been released
 */
int ControllerDevice::isReleased ( int button ) {
	return this->state.Released & button;
}

/**
 * Frees up resources held by this controller class.
 */
void ControllerDevice::cleanup() {
	if ( this->flags & PAD_INIT_PRESSURE ) {
		disablePressure();
	}

	setMode(0, 0);
	waitForStatusReady();
	closeHandle();
}

void ControllerDevice::DEBUG_ON () {
	this->debugState = 1;
}

void ControllerDevice::DEBUG_OFF () {
	this->debugState = 0;
}

/**
 * Sets the controller mode.
 * @param mode The controller mode
 * @param lock 1 to lock the controller
 */
int ControllerDevice::setMode ( int mode, int lock ) {
	struct ps2pad_mode pmode;
	pmode.offs = mode;
	pmode.lock = lock ? 3 : 2;

	return (ioctl( this->fileHandle, PS2PAD_IOCSETMODE, &pmode ) == 0);
}

void ControllerDevice::enableActuator ( int small, int big ) {
	struct ps2pad_act actuator;
	actuator.len = 6;

	memset( actuator.data, -1, sizeof(actuator.data) );

	if ( small && big ) {
		actuator.data[0] = 0;
		actuator.data[1] = 1;
	}
	else if ( small ) {
		actuator.data[0] = 0;
	}
	else if ( big ) {
		actuator.data[1] = 0;
	}

	ioctl( this->fileHandle, PS2PAD_IOCSETACTALIGN, &actuator );
}

void ControllerDevice::setActuator ( unsigned char small, unsigned char big ) {
	struct ps2pad_act actuator;
	actuator.len = 6;

	memset(actuator.data, -1, sizeof(actuator.data));

	if ( small > 1 ) {
		small = 1;
	}

	actuator.data[0] = small;
	actuator.data[1] = big;

	ioctl( this->fileHandle, PS2PAD_IOCSETACT, &actuator );
}

/**
 * Starts reading pressure data for the controller buttons.
 */
int ControllerDevice::enablePressure () {
	return ( ioctl( this->fileHandle, PS2PAD_IOCENTERPRESSMODE ) == 0 );
}

/**
 * Stops reading pressure data for the controller buttons.
 */
int ControllerDevice::disablePressure () {
	return ( ioctl( this->fileHandle, PS2PAD_IOCEXITPRESSMODE ) == 0 );
}

/**
 * Checks if the actuator is supported on this controller
 * @return 1 if the actuator is supported
 */
int ControllerDevice::isActuatorSupported () {
	struct ps2pad_actinfo actuatorInfo;
	actuatorInfo.actno = -1;
	actuatorInfo.term = 0;
	actuatorInfo.result = 0;

	ioctl(this->fileHandle, PS2PAD_IOCACTINFO, &actuatorInfo);

	return (actuatorInfo.result == 2);
}

/**
 * Checks if pressure data is supported on this controller.
 * @return 1 if pressure data is supported, 0 otherwise
 */
int ControllerDevice::isPressureSupported () {
	int pressmode;
	ioctl(this->fileHandle, PS2PAD_IOCPRESSMODEINFO, &pressmode);

	return (pressmode == 1);
}

int ControllerDevice::getStatus () {
	int res;
	ioctl(this->fileHandle, PS2PAD_IOCGETSTAT, &res);
	return res;
}

int ControllerDevice::waitForStatusReady () {
	int res;
	while ( (res = getStatus()) == PS2PAD_STAT_BUSY ) {}
	return res;
}

void ControllerDevice::log ( const char *message ) {
	if ( this->debugState ) {
		printf("CONTROLLER %i: %s\n", this->port, message);
	}
}

/**
 * @private
 * Closes the linux file handle to the controller port.
 */
int ControllerDevice::closeHandle () {
	close(this->fileHandle);
	this->fileHandle = -1;

	return 0;
}

/**
 * Converts a pressure reading from a button into a float.
 * @param pressure The pressure reading to convert
 * @return The float representing the button pressure
 */
float ControllerDevice::pressure2float ( int pressure ) {
	return ((float)pressure) * (1.0f / 255.0f);
}

/**
 * Converts an axis reading from the analogue stick into a float.
 * @param axis The axis reading from the controller to convert
 * @return The float representing the axis state
 */
float ControllerDevice::axis2float ( int axis ) {
	if ( abs( axis - 127 ) < DRIFT_TOLERANCE ) {
		return 0.0f;
	}

	return (((float)axis) - 127.5f) * (1.0f / 127.5f);
}
