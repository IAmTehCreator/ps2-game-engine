#ifndef __CONTROLLER_DEVICE_HEADER__
#define __CONTROLLER_DEVICE_HEADER__

typedef struct {
	/** @prop InitFlags Flags used to initialise the controller state. */
	int InitFlags;

	/** @prop Actuator Value is 1 if the actuator is supported and 0 if it is not. */
	int Actuator;

	/** @prop Buttons A bitfield describing which buttons are currently pressed. */
	int Buttons;

	/** @prop Axes An array of analogue stick axis measurements from -1 -> +1 */
	float Axes[4];

	/** @prop Pressures An array of preassure value for each button mapped from 0 -> 1 */
	float Pressures[12];

	/** @prop Pressed A bitfield describing which buttons were pressed since the last update. */
	int Pressed;

	/** @prop Released A bitfield describing which buttons were released since the last update. */
	int Released;
} ControllerState;

class ControllerDevice {
public:
	ControllerDevice();
	~ControllerDevice();

	int init ( int port, int flags );
	void update ();
	ControllerState getState ();

	int isPressed ( int button );
	int isDown ( int button );
	int isReleased ( int button );

	void cleanup ();

	void DEBUG_ON ();
	void DEBUG_OFF ();
private:
	ControllerState state;
	int debugState;
	int fileHandle;
	int flags;
	int port;

	int setMode ( int mode, int lock );
	void enableActuator ( int small, int big );
	void setActuator ( unsigned char small, unsigned char big );
	int enablePressure ();
	int disablePressure ();

	int isActuatorSupported ();
	int isPressureSupported ();

	int getStatus ();
	int waitForStatusReady ();

	void log ( const char *message );

	// Utility methods
	int closeHandle ();
	float pressure2float ( int pressure );
	float axis2float ( int axis );
};

//bitmasks for digital button presses
#define PAD_SELECT	(1 << 0)
#define PAD_L3		(1 << 1)
#define PAD_R3		(1 << 2)
#define PAD_START	(1 << 3)
#define PAD_UP		(1 << 4)
#define PAD_RIGHT	(1 << 5)
#define PAD_DOWN	(1 << 6)
#define PAD_LEFT	(1 << 7)
#define PAD_L2		(1 << 8)
#define PAD_R2		(1 << 9)
#define PAD_L1		(1 << 10)
#define PAD_R1		(1 << 11)
#define PAD_TRI		(1 << 12)
#define PAD_CIRCLE	(1 << 13)
#define PAD_CROSS	(1 << 14)
#define PAD_SQUARE	(1 << 15)

//indices into axes
#define PAD_AXIS_LX	0
#define PAD_AXIS_LY	1
#define PAD_AXIS_RX	2
#define PAD_AXIS_RY	3

//indices into pressures
#define PAD_PLEFT	0
#define PAD_PRIGHT	1
#define PAD_PUP		2
#define PAD_PDOWN	3
#define PAD_PTRI	4
#define PAD_PCIRCLE	5
#define PAD_PCROSS	6
#define PAD_PSQUARE	7
#define PAD_PL1		8
#define PAD_PR1		9
#define PAD_PL2		10
#define PAD_PR2		11

//use these with the pad_* functions
#define PAD_0		0
#define PAD_1		1

//pad init flags
#define PAD_INIT_DIGITAL	0x00	//select digital mode (default)
#define PAD_INIT_ANALOGUE	0x01	//select analogue mode
#define PAD_INIT_UNLOCK		0x00	//do not lock the pad mode
#define PAD_INIT_LOCK		0x02	//lock the pad mode
#define PAD_INIT_PRESSURE	0x04	//enable button pressure sensing

#endif
