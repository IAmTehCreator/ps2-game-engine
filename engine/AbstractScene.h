#ifndef __ABSTRACT_SCENE_HEADER__
#define __ABSTRACT_C__ABSTRACT_SCENE_HEADER__ONTROLLER_HEADER__

// TODO: Extract class
// Actors represent a sprite along with controller logic
class Actor {};

class AbstractScene {
public:
	AbstractScene ();
	~AbstractScene ();

	Actor lookupActor (char *id);
protected:
private:
};

#endif
