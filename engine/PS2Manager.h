#ifndef __PS2_MANAGER_HEADER__
#define __PS2_MANAGER_HEADER__

#define PS2 PS2Manager::getSingleton()

#include "Singleton.h"
#include "devices/ControllerDevice.h"
#include "devices/VectorUnitDevice.h"

typedef struct {
	int ControllerCount;			// The number of controller ports to initialise
	int ControllerFlags;			// The flags to initialise the controllers with
} Ps2InitConfig;

class PS2Manager : public Singleton<PS2Manager> {
public:
	PS2Manager ();
	~PS2Manager ();

	void init ( Ps2InitConfig config );

	VectorUnitDevice getVU1 ();
	ControllerDevice getController ( int port );

	void cleanup ();
private:
	VectorUnitDevice vu1;

	ControllerDevice controller0;
	ControllerDevice controller1;
};

#endif
