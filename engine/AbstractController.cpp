#include "AbstractController.h"

/**
 * Abstract controller constructor.
 * @param controllerPort The physical controller that this controller should listen to
 */
AbstractController::AbstractController ( ControllerDevice *device ) {
	this->device = device;
}

AbstractController::~AbstractController () {}

/**
 * Called each frame to provide a logic tick
 */
void AbstractController::tick () {
	if ( this->device->isPressed(PAD_UP) ) { onUpPressed(); }
	if ( this->device->isPressed(PAD_RIGHT) ) { onRightPressed(); }
	if ( this->device->isPressed(PAD_DOWN) ) { onDownPressed(); }
	if ( this->device->isPressed(PAD_LEFT) ) { onLeftPressed(); }
	if ( this->device->isPressed(PAD_SELECT) ) { onSelectPressed(); }
	if ( this->device->isPressed(PAD_START) ) { onStartPressed(); }
	if ( this->device->isPressed(PAD_TRI) ) { onTrianglePressed(); }
	if ( this->device->isPressed(PAD_CIRCLE) ) { onCirclePressed(); }
	if ( this->device->isPressed(PAD_CROSS) ) { onCrossPressed(); }
	if ( this->device->isPressed(PAD_SQUARE) ) { onSquarePressed(); }
	if ( this->device->isPressed(PAD_R1) ) { onR1Pressed(); }
	if ( this->device->isPressed(PAD_R2) ) { onR2Pressed(); }
	if ( this->device->isPressed(PAD_R3) ) { onR3Pressed(); }
	if ( this->device->isPressed(PAD_L1) ) { onL1Pressed(); }
	if ( this->device->isPressed(PAD_L2) ) { onL2Pressed(); }
	if ( this->device->isPressed(PAD_L3) ) { onL3Pressed(); }

	onTick();
}

/**
 * Template methods that are called when each button is pressed
 */

void AbstractController::onTick () {}
void AbstractController::onUpPressed () {}
void AbstractController::onRightPressed () {}
void AbstractController::onDownPressed () {}
void AbstractController::onLeftPressed () {}
void AbstractController::onSelectPressed () {}
void AbstractController::onStartPressed () {}
void AbstractController::onTrianglePressed () {}
void AbstractController::onCirclePressed () {}
void AbstractController::onCrossPressed () {}
void AbstractController::onSquarePressed () {}
void AbstractController::onR1Pressed () {}
void AbstractController::onR2Pressed () {}
void AbstractController::onR3Pressed () {}
void AbstractController::onL1Pressed () {}
void AbstractController::onL2Pressed () {}
void AbstractController::onL3Pressed () {}
