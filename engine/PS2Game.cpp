#include "PS2Game.h"

#include <stdio.h>

PS2Game::PS2Game () {
	initControllers();
	initRenderer();
}

PS2Game::~PS2Game () {}

void PS2Game::run () {
	this->runState = 1;
	while ( this->runState ) {
		gameLoop();
	}
}

void PS2Game::stop () {
	this->runState = 0;
}

void PS2Game::cleanup () {
	log("Cleaning up...");
	controller0.cleanup();
	controller1.cleanup();
}

void PS2Game::DEBUG_ON () {
	this->debugState = 1;
	log("Debug mode enabled");

	controller0.DEBUG_ON();
	controller1.DEBUG_ON();
}

void PS2Game::DEBUG_OFF () {
	log("Debug mode disabled");

	controller0.DEBUG_OFF();
	controller1.DEBUG_OFF();

	this->debugState = 0;
}

int PS2Game::getControllerFlags () {
	return PAD_INIT_LOCK | PAD_INIT_ANALOGUE | PAD_INIT_PRESSURE;
}

void PS2Game::onTick () {}

void PS2Game::changeScene ( AbstractScene *scene ) {
	delete this->scene;

	this->scene = scene;
}

void PS2Game::log (const char *message) {
	if ( this->debugState ) {
		puts(message);
	}
}

void PS2Game::handleError (int condition, const char *message) {
	if ( !condition ) {
		log(message);
	}
}

void PS2Game::initControllers () {
	int controllerFlags = getControllerFlags();

	handleError( controller0.init( PAD_0, controllerFlags ), "ERROR: Could not initialize controller 0");
	handleError( controller1.init( PAD_1, controllerFlags ), "ERROR: Could not initialize controller 1");
}

void PS2Game::initRenderer () {
	// TODO: Instatiate a renderer
}

void PS2Game::gameLoop () {
	controller0.update();
	controller1.update();

	if( controller0.isPressed(PAD_SELECT) && controller0.isPressed(PAD_START) ) {
		log("Terminating at user request...");
		stop();
	}

	onTick();

	renderer();
}

void PS2Game::renderer () {
	// TODO: Call the current scenes render method with the renderer
}
