#ifndef __PS2_GAME_HEADER__
#define __PS2_GAME_HEADER__

#include "devices/ControllerDevice.h"
#include "AbstractController.h"
#include "AbstractScene.h"

class PS2Game {
public:
	PS2Game ();
	~PS2Game ();

	void run ();
	void stop ();
	void cleanup ();

	void DEBUG_ON ();
	void DEBUG_OFF ();
protected:
	ControllerDevice controller0;
	ControllerDevice controller1;

	AbstractScene *scene;

	virtual int getControllerFlags ();
	virtual void onTick ();

	void changeScene ( AbstractScene *scene );

	void log (const char *message);
	void handleError (int condition, const char *message);
private:
	int runState;
	int debugState;
	void initControllers ();
	void initRenderer ();
	void gameLoop ();
	void renderer ();
};

#endif
