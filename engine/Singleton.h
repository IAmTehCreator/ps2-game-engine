#ifndef __SINGLETON_HEADER__
#define __SINGLETON_HEADER__

template <typename T> class Singleton {
public:
	Singleton () {
		instance = static_cast<T*>(this);
	}

	~Singleton () {
		instance = 0;
	}

	static T& getSingleton () {
		return (*instance);
	}

	static T* getSingletonPtr () {
		return instance;
	}

private:
	static T *instance;
};

// Initialize the static T *instance member variable
template <typename T> T * Singleton <T>::instance = 0;

#endif
