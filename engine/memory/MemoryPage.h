#ifndef __MEMORY_PAGE_HEADER__
#define __MEMORY_PAGE_HEADER__

class MemoryPage {
public:
	MemoryPage ();
	MemoryPage (int quadWords);
	~MemoryPage ();

	void set (int amount, void *cached, void *uncached, int physAddr);

	void *getCached ();
	void *getUncached ();
	int getPhysicalAddress ();
	int getSize ();
private:
	int m_iAmount;		// Number of quadwords in this block.
	void *m_pCached;	// Cached pointer to the start of this block.
	void *m_pUncached;	// Uncached pointer to the start of this block.
	int m_iPhysAddr;	// Physical address of this start of this block.
};

#endif
