#include "VIFStaticDMA.h"

VIFStaticDMA::VIFStaticDMA () {
	this->currentPage = 0;
}

VIFStaticDMA::~VIFStaticDMA () {}

void VIFStaticDMA::initialise (int iNumPages) {
	this->pageCount = iNumPages;
	this->pages = new MemoryPage[iNumPages];

	for(int iPage = 0; iPage < iNumPages; iPage++) {
		SPS2.allocate(this->pages[iPage], 256);
	}

	*EE_VIF1_ERR = 2; // Ignore DMA mismatch error
	begin();
}

int VIFStaticDMA::getPointer () {
	return (int)this->dmaTag + this->physicalOffset;
}

int VIFStaticDMA::newPage () {
	assert(this->currentPage < m_iNumPages);

	this->pointer = (int *)this->pages[this->currentPage].getCached();
	this->endPointer = this->pointer + 1024;

	int iPhysAddr = (int)this->pages[this->currentPage++].getPhysicalAddress();
	this->physicalOffset = iPhysAddr - (int)this->pointer;

	return iPhysAddr;
}
