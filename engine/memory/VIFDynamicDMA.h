#ifndef __VIF_DYNAMIC_DMA_HEADER__
#define __VIF_DYNAMIC_DMA_HEADER__

#include "DMAChain.h"
#include "MemoryPage.h"

class VIFDynamicDMA : public DMAChain {
public:
	VIFDynamicDMA ();
	~VIFDynamicDMA ();

	void initialise (int iNumPages);
	void fire (bool bFire = true, bool bPrint = false);
	virtual void printPacket ();
	MemoryPage &getTempPage (int iPage = 0);
protected:
	virtual int newPage ();

	int currentPage;
	int currentBuffer;
};

#endif
