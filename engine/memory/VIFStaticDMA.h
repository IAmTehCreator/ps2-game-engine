#ifndef __VIF_STATIC_DMA_HEADER__
#define __VIF_STATIC_DMA_HEADER__

#include "DMAChain.h"
#include "MemoryPage.h"

class VIFStaticDMA : public DMAChain {
public:
	VIFStaticDMA ();
	~VIFStaticDMA ();

	void initialise (int iNumPages);
	int getPointer ();
protected:
	virtual int newPage ();

	int currentPage;
	int physicalOffset;
};

#endif
