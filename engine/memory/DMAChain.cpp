#include "DMAChain.h"

DMAChain::DMAChain () {
	this->pages = 0;
}

DMAChain::~DMAChain () {
	// Delete the array that holds the SPS2 memory pages (note we aren't freeing up the SPS2 memory, just this array)
	if (this->pages) {
		delete [] this->pages;
		this->pages = 0;
	}
}

void DMAChain::printPacket () {
	SPS2.flushCache();

	unsigned int *pMem = (unsigned int *)this->pages[0].getCached();

	printf("            ___________________________________________ \n");
	printf("  Address  |127     96|95      64|63      32|31       0| Virt Addr\n");
	printf(".----------+----------+----------+----------+----------|----------.\n");

	int iWritten = ((int)this->pointer - (int)pMem + 0xF) >> 4;

	for (int i = 0; i < iWritten; i++) {
		static int iPhysAddr = 0;
		if ((i % 256) == 0) {
			iPhysAddr = this->pages[(i / 256)].getPhysicalAddress();
		}

		printf("| %.8X | ", iPhysAddr + ((i % 256) << 4));

		for (int j = 0; j < 4; j++) {
			printf("%.8X | ", pMem[3 - j]);
		}

		printf("%.8X | ", (int)pMem);

		printf("\n");
		pMem += 4;
	}

	printf("'-----------------------------------------------------------------'\n");
}

void DMAChain::stitch () {
	VIFState vifState = this->vifState;

	// If we are in direct mode we will have to end it, stitch, and then start it again in the new page
	if (vifState == VIF_DIRECT) {
		endDirect();
	}

	// Finish the previous DMA tag (i.e. set its QWC) and point it to the start of the next page
	*this->dmaTag = ((uint64)newPage() << 32) + (2 << 28) + (this->pointer - (int *)this->dmaTag) / 4 - 1;

	// And start a new one.
	newTag();

	if (vifState == VIF_DIRECT) {
		startDirect();
	}
}

void DMAChain::newTag () {
	this->dmaTag = (uint64 *)this->pointer;
	add64(0);
}

void DMAChain::prepForDMATag ()
{
	// Make sure we aren't in DIRECT mode
	assert(this->vifState == VIF_BASE);

	// Make sure we are aligned on a quadword boundary.
	align(4);

	// We can't add a new tag at the very end of a packet, so lets just add a NOP and let the stitching handle the new tag.
	if(this->pointer == this->endPointer)
		add64(0);
}

void DMAChain::endDMA () {
	assert(this->vifState == VIF_BASE);

	// Pad if we are trying to transfer less than a whole number of quadwords
	align(4);

	// And finish the open DMA tag.
	*this->dmaTag = (7 << 28) + (this->pointer - (int *)this->dmaTag) / 4 - 1;
}

void DMAChain::DMAReturn () {
	prepForDMATag();

	// Put the ret tag in as the most recent open DMA tag.
	*this->dmaTag = (6 << 28) + (this->pointer - (int *)this->dmaTag) / 4 - 1;

	// And create a new tag.
	newTag();
}

void DMAChain::DMACall (uint32 iAddr) {
	prepForDMATag();

	*this->dmaTag = ((uint64)iAddr << 32) + (5 << 28) + (this->pointer - (int *)this->dmaTag) / 4 - 1;
	newTag();
}

void DMAChain::startDirect () {
	assert(this->vifState == VIF_BASE);

	// Remember that the DIRECT vif code has to go into the very last (i.e. 3rd) word of a quadword.
	align(4, 3);

	// Set the vif state so that the stitching knows if it has to stop and start direct mode.
	this->vifState = VIF_DIRECT;
	this->vifCode = this->pointer;

	// Add the space for the DIRECT vif code (it won't be written until EndDirect() is called and we
	// know how many quad words were written)
	add32(0);
}

void DMAChain::endDirect () {
	assert(this->vifState == VIF_DIRECT);

	// We can only transfer an integer number of quadwords, so pad with NOPs if we don't haven't got that much.
	align(4);

	int iSize = (this->pointer - this->vifCode - 1) / 4;
	if(iSize != 0)	// If the size isn't 0, set write the DIRECT vifcode.
		*this->vifCode = (0x50 << 24) + iSize;

	this->vifState = VIF_BASE;
}

void DMAChain::startAD () {
	assert(this->vifState == VIF_DIRECT);

	if (this->pointer >= this->endPointer) {
		endDirect();
		startDirect();
	}

	// Store the location so we can add the A+D amount later.
	this->adGifTag = (uint64 *)this->pointer;

	// Add the A+D giftag
	add64((1 << 15) | (1ULL << 60));
	add64(0xE);
}

void DMAChain::addAD (uint64 iData, uint64 iAddr) {
	add64(iData);
	add64(iAddr);

	// Increment the NLOOP in the giftag
	(*this->adGifTag)++;
}

void DMAChain::startMicrocodeUpload (uint32 iAddr) {
	assert(this->vifState == VIF_BASE);

	// MPG vifcode must be in either word 1 or 3 of a quadword
	align(2, 1);
	this->vifCode = this->pointer;

	// Add the VIF code
	add32((0x4a << 24) + iAddr);

	this->mpgCount = 0;
	this->mpgAddress = iAddr;
}

void DMAChain::endMicrocodeUpload () {
	// Write the number of doubleword chunks of VU code that were written, to the MPG vifcode.
	*this->vifCode += (this->mpgCount & 0xFF) << 16;
}

void DMAChain::addMicrocode (uint64 iInst) {
	// We are only allowed to transfer 256 microcode instructions at once, so if we are trying to send
	// more we will need to use a new MPG vifcode.
	if (this->mpgCount >= 256) {
		endMicrocodeUpload();
		startMicrocodeUpload(this->mpgAddress + this->mpgCount);
	}

	// Add the instruction and increment the microcode count.
	add64(iInst);
	this->mpgCount++;
}

void DMAChain::align (int iAlign, int iOffset = 0) {
	int p = (((int)this->pointer >> 2) - iOffset) & (iAlign - 1);

	if(p == 0)
		return;

	for(p = iAlign - p; p > 0; p--)
		add32(0);
}

void DMAChain::addVector (float x, float y, float z, float w) {
	addFloat(x);
	addFloat(y);
	addFloat(z);
	addFloat(w);
}

void DMAChain::addVector(int x, int y, int z, int w) {
	add32(x);
	add32(y);
	add32(z);
	add32(w);
}

void DMAChain::addVector (const Vector4 &vector) {
	addFloat( vector.x );
	addFloat( vector.y );
	addFloat( vector.z );
	addFloat( vector.w );
}

void DMAChain::addMatrix (const Matrix4x4 &matrix) {
	addFloat( matrix(0, 0) );
	addFloat( matrix(0, 1) );
	addFloat( matrix(0, 2) );
	addFloat( matrix(0, 3) );

	addFloat( matrix(1, 0) );
	addFloat( matrix(1, 1) );
	addFloat( matrix(1, 2) );
	addFloat( matrix(1, 3) );

	addFloat( matrix(2, 0) );
	addFloat( matrix(2, 1) );
	addFloat( matrix(2, 2) );
	addFloat( matrix(2, 3) );

	addFloat( matrix(3, 0) );
	addFloat( matrix(3, 1) );
	addFloat( matrix(3, 2) );
	addFloat( matrix(3, 3) );
}

void DMAChain::begin() {
	this->vifState = VIF_BASE;
	this->physicalAddress = newPage();

	newTag();
}

void DMAChain::add32 (uint32 data) {
	// Would we be writing to the new page?
	if (m_pPtr >= m_pEndPtr) {
		// If so then stitch
		stitch();
	}

	*this->pointer++ = data;		// Write the data and then move the pointer on
}

void DMAChain::add64 (uint64 data) {
	add32((uint32)data);
	add32((uint32)(data >> 32));
}

void DMAChain::add128 (uint128 data) {
	add64((uint64)data);
	add64((uint64)(data >> 64));
}
