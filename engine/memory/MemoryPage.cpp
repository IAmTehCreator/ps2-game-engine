#include <stdio.h>
#include "MemoryPage.h"

MemoryPage::MemoryPage () : m_iAmount(0), m_pCached(NULL), m_pUncached(NULL), m_iPhysAddr(0) {};
MemoryPage::MemoryPage (int quadWords) : m_iAmount(0), m_pCached(NULL), m_pUncached(NULL), m_iPhysAddr(0) {
	// TODO: Hook up to SPS2Lib
	//SPS2Manager.Allocate(*this, quadWords);
}

MemoryPage::~MemoryPage () {}

void MemoryPage::set (int amount, void *cached, void *uncached, int physAddr) {
	m_iAmount = amount;
	m_pCached = cached;
	m_pUncached = uncached;
	m_iPhysAddr = physAddr;
}

void *MemoryPage::getCached () {
	return m_pCached;
}

void *MemoryPage::getUncached () {
	return m_pUncached;
}

int MemoryPage::getPhysicalAddress () {
	return m_iPhysAddr;
}

int MemoryPage::getSize () {
	return m_iAmount;
}
