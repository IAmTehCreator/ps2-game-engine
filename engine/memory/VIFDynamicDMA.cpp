#include "VIFDynamicDMA.h"

VIFDynamicDMA::VIFDynamicDMA () {
	this->currentPage = 0;
	this->currentBuffer = 0;
}

VIFDynamicDMA::~VIFDynamicDMA () {}

void VIFDynamicDMA::initialise (int iNumPages) {
	this->pageCount = iNumPages;

	// We are using a double buffer so allocate 2x the memory
	this->pages = new MemoryPage[this->pageCount * 2];
	for(int iPage = 0; iPage < (this->pageCount * 2); iPage++) {
		SPS2.allocate(this->pages[iPage], 256);
	}

	*EE_VIF1_ERR = 2; // Ignore DMA mismatch error

	begin();
}

void VIFDynamicDMA::fire (bool bFire = true, bool bPrint = false) {
	endDMA();

	if (bPrint) {
		printPacket();
	}

	asm("sync.l");
	// Flush all the data through to memory.
	SPS2.flushCache();

	// Wait for channel 1 to finish the previous packet
	while(*EE_D1_CHCR & 256);
	*EE_D1_QWC = 0;
	*EE_D1_TADR = this->physicalAddress & 0x8ffffff0;

	// Send the packet!
	if (bFire) {
		*EE_D1_CHCR = 0x145;
	}

	asm("sync.l");

	// Make it so that now we write to the beginning of the other buffer.
	this->currentBuffer ^= 1;
	this->currentPage = 0;

	begin();
}

void VIFDynamicDMA::printPacket () {
	SPS2.flushCache();

	unsigned int * pMem = (unsigned int *)this->pages[(this->currentBuffer * this->pageCount)].GetCached();

	printf("            ___________________________________________            \n");
	printf("  Address  |127     96|95      64|63      32|31       0| Virt Addr \n");
	printf(".----------+----------+----------+----------+----------|----------.\n");

	int iWritten = ((int)this->pointer - (int)pMem + 0xF) >> 4;

	for (int i = 0; i < iWritten; i++) {
		static int iPhysAddr = 0;

		if ((i % 256) == 0) {
			iPhysAddr = this->pages[(i / 256) + (this->currentBuffer * this->pageCount)].getPhysicalAddress();
		}

		printf("| %.8X | ", iPhysAddr + ((i % 256) << 4));
		for (int j = 0; j < 4; j++) {
			printf("%.8X | ", pMem[3 - j]);
		}

		printf("%.8X | ", (int)pMem);

		printf("\n");
		pMem += 4;
	}

	printf("'-----------------------------------------------------------------'\n");
}

MemoryPage &VIFDynamicDMA::getTempPage (int iPage = 0) {
	return this->pages[((this->currentBuffer ^ 1) * this->pageCount) + iPage];
}

int VIFDynamicDMA::newPage () {
	assert(this->currentPage < this->pageCount);

	this->pointer = (int *)this->pages[this->currentPage + (this->currentBuffer * this->pageCount)].GetCached();
	this->endPointer = this->pointer + 1024;

	int iPhysAddr = (int)this->pages[this->currentPage + (this->currentBuffer * this->pageCount)].GetPhysicalAddr();
	++this->currentPage;

	return iPhysAddr;
}
