#ifndef __DMA_CHAIN__
#define __DMA_CHAIN__

#include "../types/Vector4.h"
#include "../types/Matrix4x4.h"
#include "MemoryPage.h"
#include "../SPS2Wrapper.h"

typedef char int8;
typedef unsigned char uint8;
typedef short int16;
typedef unsigned short uint16;
typedef int int32;
typedef unsigned int uint32;
typedef long long int64;
typedef unsigned long long uint64;
typedef int int128 __attribute__ ((mode(TI), aligned(16)));
typedef unsigned int uint128 __attribute__ ((mode(TI), aligned(16)));

class DMAChain {
public:
	DMAChain ();
	~DMAChain ();

	void begin ();

	void add32 (uint32 data);
	void add64 (uint64 data);
	void add128 (uint128 data);
	void addFloat (float data);
	void addVector (int x, int y, int z, int w);
	void addVector (float x, float y, float z, float w);
	void addVector (const Vector4 &vector);
	void addMatrix (const Matrix4x4 & Matrix);

	void align (int iAlign, int iOffset = 0);
	void prepForDMATag ();

	void DMAReturn ();
	void DMACall (uint32 iAddr);
	void endDMA ();

	void startDirect ();
	void endDirect ();

	void startAD ();
	void addAD (uint64 iData, uint64 iAddr);
	void endAD ();

	void startMicrocodeUpload ();
	void addMicrocode (uint64 iInst);
	void endMicrocodeUpload ();

	void addUnpack (int format, int addr, int num, int usetops = 0, int nosign = 1, int masking = 0);

	virtual void printPacket ();
protected:
	virtual int newPage() = 0;
	virtual void stitch();
	virtual void newTag();

	int pageCount;				// How many pages we have allocated for DMAing
	MemoryPage *pages;			// All the pages that we have allocated

	int *pointer;				// The data write pointer
	int *endPointer;			// The next page starts here
	int physicalAddress;		// The physical address of the first page (for TADR)

	uint64 *dmaTag;				// The currently open DMA tag
	int *vifCode;				// The currently open VIF code

	int mpgCount;				// How many MPG instructions we have written
	int mpgAddress;				// The VU_MICRO_MEM address we are writing to

	uint64 *adGifTag;			// The currently open AD GIFtag.

	typedef enum {
		VIF_BASE,
		VIF_DIRECT				// Direct + TTE has alignment issues, MPG, and unpack do not
	} VIFState;				// The different states the DMA chain can be in

	VIFState vifState;		// The VIF state at this point in the chain
};

#endif
