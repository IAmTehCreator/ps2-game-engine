#ifndef __VU_MICROPROGRAM_HEADER__
#define __VU_MICROPROGRAM_HEADER__

class VUMicroprogram {
public:
	VUMicroprogram ();
	~VUMicroprogram ();

	void upload ();
	int getLengthDW ();
private:
	int offset;
	int uploadPointer;
	int size;
};

#endif
