#include <stdio.h>
#include <fcntl.h>
#include "Game.h"

int main () {
	Game game = Game();
	game.DEBUG_ON();

	game.run();
	game.cleanup();

	return 0;
}
