# PS2 Game engine
This repository holds a simple games engine for the Playstation 2 Linux Runtime Environment.

## Dependencies
To use this games engine you must have the official Sony Linux Version 1.00 installed on a retail Playstation 2 unit. This game engine uses the [sps2](http://ps2linux.no-ip.info/playstation2-linux.com/projects/sps2.html) library and requires the kernel module be installed and the headers placed in the `/usr/include` directory.

## Developing Software on PS2 Linux
When developing software on the Playstation 2 using the Linux Kit it is advised to write the software on another computer and send it to the PS2 for testing. This is a guide to setting up a development environment for the PS2. Regardless of which platform you are using to write the software you will need to setup a few things on the PS2 first;

You must enable the SSH daemon before you will be able to connect to the PS2 from another computer. To enable the SSH daemon type the following command at the prompt;

```bash
chkconfig sshd on
service sshd start
```

This should ensure the SSH service is started on boot for the relevant kernel runtime levels. If developing with the Mac then the setup for the PS2 ends here and you can skip on to the Mac OS X section. If however you are developing with Windows then you will need to enable the Samba fileserver.

To enable Samba you must use similar commands to before when SSH was enabled. Type the following commands to enable the Samba fileserver;

```bash
chkconfig smb on
samba start
```

### Mac OS X
When developing with a Mac you can use SSH to communicate with the PS2. SSH can be used to both send console commands to the PS2 and to provide filesystem access. Mac OS X comes with the `ssh` command by default so we can easily connect to the PS2 with the following command in the terminal;

```bash
ssh user@192.168.1.6
```

Substitute `user` in the command above with your PS2 username and `192.168.1.6` with your PS2's IP address. This command will connect to the PS2, login as the specified user and give you access to Bash. Now you can type terminal commands on the Mac and have them sent to the PS2, this will be used to compile source code and run the result.

We must first be able to send our source code to the PS2 before we can compile and run it. We will mount the PS2's root filesystem on the Mac using SSH. Before we do this though you will need to install the `sshfs` utility, this can be installed through `brew` like so;

```bash
brew install sshfs
```

Once you have installed `sshfs` you can mount the PS2 with the following command in the terminal;

```bash
sshfs user@192.168.1.6:/ /Volumes/PS2
```

Again you should replace the username and IP address with the relevant details. You can mount the filesystem where ever you want, in this example it was mounted as `/Volumes/PS2`. You can now copy files from the Mac on to the PS2 using this mount point.

### Windows

## Using the Game Engine

### Example

```cpp
class Game : public PS2Game {

};

int main () {
	Game game = Game();

	game.init();
	game.run();
	game.cleanup();

	return 0;
}
```
